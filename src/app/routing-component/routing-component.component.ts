import { Component, OnInit, HostListener } from '@angular/core';
import { MobileDesktopIdentifierServiceService } from '../services/mobile-desktop-identifier-service.service';
import { FirestoreService } from '../services/firestore.service';
declare var $: any;

@Component({
  selector: 'app-routing-component',
  templateUrl: './routing-component.component.html',
  styleUrls: ['./routing-component.component.css']
})
export class RoutingComponentComponent implements OnInit {
  collapseFlag: boolean = true;
  isMobile: boolean = false;

  constructor(private mobileDesktopService: MobileDesktopIdentifierServiceService, public firestoreService: FirestoreService) { }

  ngOnInit() {
    this.scrollToTop();
    this.isMobile = this.mobileDesktopService.isMobile();
    this.fixFooterBottom();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.isMobile = this.mobileDesktopService.isMobile();
    this.fixFooterBottom();
  }

  @HostListener("window:scroll", [])
  onScroll(): void {
    const style = 'bottom: ' + (-1 * this.getScrollOffsets() + 10) + 'px;';
    $('#btn-scroll-top').attr("style", style)
  }

  toggleCollapse() {
    if (!this.collapseFlag) {
      document.getElementById("navbar-main").classList.add('collapsed');
      document.getElementById("menu-bars").classList.remove('change');
      var titles = [].slice.call(document.getElementsByClassName("navbar-element-title"));
      titles.forEach(element => {
        element.classList.add('navbar-element-title-collapse');
      });
    } else  {
      document.getElementById("navbar-main").classList.remove('collapsed');
      document.getElementById("menu-bars").classList.add('change');
      var titles = [].slice.call(document.getElementsByClassName("navbar-element-title"));
      titles.forEach(element => {
        element.classList.remove('navbar-element-title-collapse');
      });

    }
    this.collapseFlag = !this.collapseFlag
  }

  mobileSignOut() {
    this.firestoreService.signOut().then(() => {
      this.toggleCollapse();
    })
  }

  fixFooterBottom() {
    const style = 'min-height: ' + 'calc(100vh - ' + this.getFooterHeight() + ')';
    
    $("#container-main-content").attr("style", (style))
  }

  getFooterHeight() {
    return $("#footer").outerHeight() + 'px'
  }

  scrollToTop() {
    window.scroll(0,0);  
  }

  getScrollOffsets() {
    var doc = document, w = window;
    var y, docEl;
    
    if ( typeof w.pageYOffset === 'number' ) {
        y = w.pageYOffset;
    } else {
        docEl = (doc.compatMode && doc.compatMode === 'CSS1Compat')?
                doc.documentElement: doc.body;
        y = docEl.scrollTop;
    }
    return y;
  }
}
