import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaterFireMoldComponent } from './water-fire-mold.component';

describe('WaterFireMoldComponent', () => {
  let component: WaterFireMoldComponent;
  let fixture: ComponentFixture<WaterFireMoldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaterFireMoldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaterFireMoldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
