import { Component, OnInit, HostListener } from '@angular/core';
import { MobileDesktopIdentifierServiceService } from '../services/mobile-desktop-identifier-service.service'
declare var $: any;

@Component({
  selector: 'app-water-fire-mold',
  templateUrl: './water-fire-mold.component.html',
  styleUrls: ['./water-fire-mold.component.css']
})
export class WaterFireMoldComponent implements OnInit {
  isMobile: boolean;

  constructor(private mobileDesktopService: MobileDesktopIdentifierServiceService) { }


  ngOnInit() {
    window.scroll(0,0);
    $('#navbar-item-mobile-water').addClass('active');
    this.isMobile = this.mobileDesktopService.isMobile();
  }

  ngOnDestroy(): void {
    $('#navbar-item-mobile-water').removeClass('active');
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.isMobile = this.mobileDesktopService.isMobile();
  }
}
