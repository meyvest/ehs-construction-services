import { Component, OnInit, HostListener } from '@angular/core';
import { MobileDesktopIdentifierServiceService } from '../services/mobile-desktop-identifier-service.service';
import { FirestoreService } from '../services/firestore.service';
import {  Observable } from 'rxjs';
declare var $: any;

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css']
})
export class BlogsComponent implements OnInit {
  isMobile: boolean;
  blogs: Observable<any[]>;

  constructor(private mobileDesktopService: MobileDesktopIdentifierServiceService, private firestoreService: FirestoreService) { }

  ngOnInit() {
    window.scroll(0,0);
    $('#navbar-item-mobile-blogs').addClass('active');
    this.isMobile = this.mobileDesktopService.isMobile();
    this.blogs = this.firestoreService.getBlogsWithIDs();
  }

  ngOnDestroy(): void {
    $('#navbar-item-mobile-blogs').removeClass('active');
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.isMobile = this.mobileDesktopService.isMobile();
  }

  getBlogDescription(desc: string) {
    if (desc.length <= 100) {
      return desc;
    } else {
      return desc.substr(0, 100) + '...';
    }
  }


}
