import { Component, OnInit, HostListener } from '@angular/core';
import { MobileDesktopIdentifierServiceService } from '../services/mobile-desktop-identifier-service.service'
declare var $: any;

@Component({
  selector: 'app-company-services',
  templateUrl: './company-services.component.html',
  styleUrls: ['./company-services.component.css']
})
export class CompanyServicesComponent implements OnInit {
  isMobile: boolean;

  constructor(private mobileDesktopService: MobileDesktopIdentifierServiceService) { }

  ngOnInit() {
    window.scroll(0,0);
    $('#navbar-item-mobile-services').addClass('active');
    this.isMobile = this.mobileDesktopService.isMobile();
  }

  ngOnDestroy(): void {
    $('#navbar-item-mobile-services').removeClass('active');
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.isMobile = this.mobileDesktopService.isMobile();
  }

}
