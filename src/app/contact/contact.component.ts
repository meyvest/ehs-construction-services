import { Component, OnInit, HostListener } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MobileDesktopIdentifierServiceService } from '../services/mobile-desktop-identifier-service.service';
import { FirestoreService } from '../services/firestore.service'
declare var $: any;

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  isMobile: boolean;
  form: FormGroup;
  comment: string;
  isContactSent: boolean = false;

  constructor(private mobileDesktopService: MobileDesktopIdentifierServiceService, private formBuilder: FormBuilder, private firestoreService: FirestoreService) { }

  ngOnInit() {
    window.scroll(0,0);
    $('#navbar-item-mobile-contact').addClass('active');
    this.isMobile = this.mobileDesktopService.isMobile();
    this.form = this.formBuilder.group({
      first_name: [null, Validators.required],
      last_name: [null, Validators.required],
      company_name: [null, Validators.required],
      email: [null, Validators.required],
      phone: [null, [Validators.required, Validators.pattern(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)]],
      comments: [null, Validators.required],
      read: [false],
      time_created: [new Date()]
    })
  }

  ngOnDestroy(): void {
    $('#navbar-item-mobile-contact').removeClass('active');
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.isMobile = this.mobileDesktopService.isMobile();
  } 

  showSubmitButton() {
    return this.form.controls['first_name'].valid && this.form.controls['last_name'].valid && this.form.controls['company_name'].valid && this.validateEmail(this.form.controls['email'].value) && this.isValidComment() && this.form.controls['phone'].valid ;
  }

  validateEmail(email) {
    var regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    return regexp.test(String(email).toLowerCase());
  }

  updateComment() {
    this.comment = (<HTMLInputElement>document.getElementById("inp-comment")).value
  }

  isValidComment() {
    return this.comment;
  }

  onSubmit() {
    var submitData = this.form.value;
    submitData['comments'] = this.comment;
    this.firestoreService.addContact(submitData).then(() => {
      $('#btn-submit-contact').hide();
      this.isContactSent = true;
    });
  }
}
