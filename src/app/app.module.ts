import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { RoutingComponentComponent } from './routing-component/routing-component.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { WaterFireMoldComponent } from './water-fire-mold/water-fire-mold.component';
import { BlogsComponent } from './blogs/blogs.component';
import { ContactComponent } from './contact/contact.component';
import { CompanyServicesComponent } from './company-services/company-services.component';
import { SingleBlogComponent } from './single-blog/single-blog.component';
import { AdminBlogsComponent } from './admin-blogs/admin-blogs.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth'
import { environment } from 'src/environments/environment';
import { AdminContactComponent } from './admin-contact/admin-contact.component';
import { ContactPipe } from './pipes/contact.pipe';
import { BlogsPipe } from './pipes/blogs.pipe';
import { AdminSubscribersComponent } from './admin-subscribers/admin-subscribers.component';
import { SubscribersPipe } from './pipes/subscribers.pipe';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'about', component: AboutComponent},
  {path: 'services', component: CompanyServicesComponent},
  {path: 'water-fire-mold', component: WaterFireMoldComponent},
  {path: 'blogs', component: BlogsComponent},
  {path: 'blogs/details/:id', component: SingleBlogComponent},
  {path: 'contact', component: ContactComponent},
  {
    path: 'admin',
    children: [
      {path: 'blogs', component: AdminBlogsComponent},
      {path: 'contact', component: AdminContactComponent},
      {path: 'subscribers', component: AdminSubscribersComponent}
    ]
  }
]

export const firebaseConfig = environment.firebase;

@NgModule({
  declarations: [
    AppComponent,
    RoutingComponentComponent,
    HomeComponent,
    AboutComponent,
    WaterFireMoldComponent,
    BlogsComponent,
    ContactComponent,
    CompanyServicesComponent,
    SingleBlogComponent,
    AdminBlogsComponent,
    AdminContactComponent,
    ContactPipe,
    BlogsPipe,
    AdminSubscribersComponent,
    SubscribersPipe
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule.enablePersistence(),
    AngularFireStorageModule,
    AngularFireAuthModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
