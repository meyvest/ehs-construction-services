import { Component, OnInit, HostListener } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirestoreService } from '../services/firestore.service';
import { MobileDesktopIdentifierServiceService } from '../services/mobile-desktop-identifier-service.service';
import {  Observable } from 'rxjs';
declare var $: any;

@Component({
  selector: 'app-admin-blogs',
  templateUrl: './admin-blogs.component.html',
  styleUrls: ['./admin-blogs.component.css']
})
export class AdminBlogsComponent implements OnInit {
  blogs: Observable<any[]>;
  blogsList: any[];
  blog: any;
  form: FormGroup;
  description: string;
  selectedFile: File;
  percentage: Observable<number>;
  searchValue = '';
  sortKey = '';
  prevSortKeySelected = '';
  isMobile: boolean = false;

  constructor(private formBuilder: FormBuilder, private mobileDesktopService: MobileDesktopIdentifierServiceService, public firestoreService: FirestoreService) { }

  ngOnInit() {
    window.scroll(0,0);
    this.blogs = this.firestoreService.getBlogsWithIDs();
    this.blogs.subscribe(data => {
      this.blogsList = data;
    });
    this.form = this.formBuilder.group({
      title: [null, Validators.required],
      description: [null, Validators.required]
    });
    this.isMobile = this.mobileDesktopService.isMobile();
  }

  setDefaultModals() {
    $('#createBlogModal').modal('toggle');
    $('#btn-delete-confirm').attr("style", "display: none !important");
    $('#btn-delete').attr("style", "display: block !important");
    $('#upload-img').val(null);
    if (this.selectedFile) {
      this.selectedFile = null;
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.isMobile = this.mobileDesktopService.isMobile();
  }


  setNewBlog() {
    this.blog = null;
    this.setFormValues('', '');
  }

  updateDescription() {
    this.description = (<HTMLInputElement>document.getElementById("inp-description")).value
  }

  isValidDescription() {
    return this.description;
  }

  onSubmit() {
    var submitData = this.form.value;
    submitData['description'] = this.description;
    if (this.selectedFile) {
      this.uploadBlogWithImage(submitData, this.selectedFile);
    } else {
      this.uploadBlog(submitData);
      $('#createBlogModal').modal('hide')
    }
  }

  uploadBlog(submitData: any) {
    if (this.blog) {
      submitData['time_modified'] = new Date();
      this.firestoreService.updateBlog(submitData, this.blog.id);
    }
    else {
      submitData['time_created'] = new Date();
      this.firestoreService.addBlog(submitData);
    }
  }

  uploadBlogWithImage(submitData: any, file: File) {
    if (this.blog) {
      submitData['time_modified'] = new Date();
      this.firestoreService.updateBlogWithImage(file, submitData, this.blog.id);
    }
    else {
      submitData['time_created'] = new Date();
      this.firestoreService.addBlogWithImage(file, submitData);
    }
    this.percentage = this.firestoreService.getPercentage();
  }

  showSubmitButton() {
    return this.form.controls['title'].valid && this.isValidDescription() ;
  }

  updateEditModal(index: any) {
    if (!this.getEditSwitchValue()) return;
    this.blog = this.blogsList[index]
    this.setFormValues(this.blog.title, this.blog.description);
    this.toggleBlogModal();
  }

  setFormValues(title: string, description: string) {
    this.form.controls['title'].setValue(title);
    (<HTMLInputElement>document.getElementById("inp-description")).value = description;
    this.description = description;
  }

  deleteBlog() {
    this.firestoreService.deleteBlog(this.blog.id);
    $('#btn-delete').attr("style", "display: block !important");
    $('#btn-delete-confirm').attr("style", "display: none !important");
  }

  setDeleteFlag() {
    $('#btn-delete').attr("style", "display: none !important");
    $('#btn-delete-confirm').attr("style", "display: block !important");
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
  }

  switchFilterKey(id: string) {
    this.sortKey = id;
    this.triggerActiveDropdown(id);
  }

  triggerActiveDropdown(id: string) {
    // console.log(id);
    if(!this.prevSortKeySelected) this.prevSortKeySelected = id;

    $('#' + this.prevSortKeySelected).removeClass('bg-primary').removeClass('text-white');
    this.prevSortKeySelected = id;
    $('#' + id).addClass('bg-primary').addClass('text-white');
  }

  onKey(event: any) { // without type info
    this.searchValue = event.target.value;
  }

  getEditSwitchValue() {
    return $('#editSwitch').is(":checked");
  }

  toggleBlogModal() {
    $('#createBlogModal').modal('toggle');
  }
}
