import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class MobileDesktopIdentifierServiceService {

  constructor() { }

  isMobile() {
    const innerWidth = window.innerWidth;
    if (innerWidth <= 767) {
      return true;
    } 
    return false;
  }
}
