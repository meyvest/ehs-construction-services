import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BlogsService {

  blogs: any[] = [
    {title: 'Branching Out!',
      description: 'Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna. Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna.Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna.Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna.',
      date: 'January 9th, 2019',
      img: 'assets/images/template-img-1.png',
      id: 0
    },
    {title: 'Project of the Month!',
      description: 'Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna. Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna.Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna.Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna.',
      date: 'December 4th, 2018',
      img: 'assets/images/template-img-2.png',
      id: 1
    },
    {title: 'First Blog!',
      description: 'Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna. Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna.Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna.Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna.',
      date: 'November 12th, 2019',
      img: 'assets/images/template-img-3.png',
      id: 2
    },
  ];
  constructor() { }

  getBlogsList(): Observable<any[]> {
    return of(this.blogs);
  }

  getBlog(id: any) {
    return of(this.blogs[id]);
  }
}
