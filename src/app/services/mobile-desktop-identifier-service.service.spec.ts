import { TestBed } from '@angular/core/testing';

import { MobileDesktopIdentifierServiceService } from './mobile-desktop-identifier-service.service';

describe('MobileDesktopIdentifierServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MobileDesktopIdentifierServiceService = TestBed.get(MobileDesktopIdentifierServiceService);
    expect(service).toBeTruthy();
  });
});
