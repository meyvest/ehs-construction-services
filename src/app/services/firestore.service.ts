import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import {  Observable, of } from 'rxjs';
import { map, finalize, switchMap } from 'rxjs/operators';
import { User } from './user.model';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {
  //Firebase Collection and Observable
  blogsCollection: AngularFirestoreCollection <any> ;
  contactCollection: AngularFirestoreCollection <any> ;
  subscribersCollection: AngularFirestoreCollection <any> ;
  blogsDocument: AngularFirestoreDocument <any> ;
  contactDocument: AngularFirestoreDocument <any>;
  subscriberDocument: AngularFirestoreDocument <any>
  task: AngularFireUploadTask;
  snapshot: Observable<any>;
  downloadURL: Observable<string>;
  image: string;
  ref: any;
  percentage: Observable<number>;
  user$: Observable<User>;

  constructor(private afs: AngularFirestore, private storage: AngularFireStorage, private afAuth: AngularFireAuth, private router: Router) { 
    this.user$ = this.afAuth.authState.pipe(
      switchMap(user => {
          // Logged in
        if (user) {
          return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          // Logged out
          return of(null);
        }
      })
    )
  }

  getBlogs(): Observable<any[]> {
    this.blogsCollection = this.afs.collection('blogs', ref => {
      return ref.orderBy('time_created').limit(40);;
    })
    return this.blogsCollection.valueChanges();
  }

  getBlogsWithIDs(): Observable<any[]> {
    this.blogsCollection = this.afs.collection('blogs', ref => {
      return ref.orderBy('time_created', 'desc').limit(40);
    })
    return this.blogsCollection.snapshotChanges().pipe(map(actions => {
      return actions.map (a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return {id, ...data};
      })
    }))
  }

  addBlog(data: any) {
    this.blogsCollection = this.afs.collection('blogs');
    return this.blogsCollection.add(data);
  }

  updateBlog(data: any, id: string) {
    this.blogsDocument = this.afs.doc('blogs/' + id);
    return this.blogsDocument.update(data);
  }

  deleteBlog(id: string) {
    this.blogsDocument = this.afs.doc('blogs/' + id);
    return this.blogsDocument.delete();
  }

  getBlog(id: string) {
    this.blogsDocument = this.afs.doc('blogs/' + id);
    return this.blogsDocument.valueChanges();
  }

  addBlogWithImage(file: File, data: any) {
    if (file.type.split('/')[0] !== 'image') { 
      console.error('unsupported file type');
      return;
    }
    const path = `images/${new Date().getTime()}_${file.name}`;
    this.ref = this.storage.ref(path);
    this.task = this.storage.upload(path, file);
    this.snapshot = this.task.snapshotChanges();
    this.percentage = this.task.percentageChanges();
    this.task.snapshotChanges().pipe(
      finalize(() => {
        this.downloadURL = this.ref.getDownloadURL()
        this.downloadURL.subscribe(url => {
          this.image = url
          data['img'] = url;
          this.addBlog(data);
        });
      })
    )
    .subscribe();
  }

  
  updateBlogWithImage(file: File, data: any, id: string) {
    if (file.type.split('/')[0] !== 'image') { 
      console.error('unsupported file type');
      return;
    }
    const path = `images/${new Date().getTime()}_${file.name}`;
    this.ref = this.storage.ref(path);
    this.task = this.storage.upload(path, file);
    this.snapshot = this.task.snapshotChanges();
    this.percentage = this.task.percentageChanges();
    this.task.snapshotChanges().pipe(
      finalize(() => {
        this.downloadURL = this.ref.getDownloadURL()
        this.downloadURL.subscribe(url => {
          this.image = url
          data['img'] = url;
          this.updateBlog(data, id);
        });
      })
    )
    .subscribe();
  }

  getContacts(): Observable<any[]> {
    this.contactCollection = this.afs.collection('contact', ref => {
      return ref.orderBy('time_created').limit(40);;
    })
    return this.contactCollection.valueChanges();
  }

  
  getContactsWithIDs(): Observable<any[]> {
    this.blogsCollection = this.afs.collection('contact', ref => {
      return ref.orderBy('time_created', 'desc').limit(40);
    })
    return this.blogsCollection.snapshotChanges().pipe(map(actions => {
      return actions.map (a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return {id, ...data};
      })
    }))
  }

  addContact(data: any) {
    this.contactCollection = this.afs.collection('contact');
    return this.contactCollection.add(data);
  }

  updateContact(data: any, id: string) {
    this.contactDocument = this.afs.doc('contact/' + id);
    return this.contactDocument.set(data, { merge: true });
  }

  deleteContact(id: string) {
    this.contactDocument = this.afs.doc('contact/' + id);
    return this.contactDocument.delete();
  }

  getSubscribers(): Observable<any[]> {
    this.blogsCollection = this.afs.collection('subscribers', ref => {
      return ref.orderBy('time_created').limit(40);;
    })
    return this.blogsCollection.valueChanges();
  }

  getSubscribersWithIDs(): Observable<any[]> {
    this.blogsCollection = this.afs.collection('subscribers', ref => {
      return ref.orderBy('time_created', 'desc').limit(40);
    })
    return this.blogsCollection.snapshotChanges().pipe(map(actions => {
      return actions.map (a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return {id, ...data};
      })
    }))
  }

  addSubscriber(data: any) {
    this.blogsCollection = this.afs.collection('subscribers');
    return this.blogsCollection.add(data);
  }

  updateSubscriber(data: any, id: string) {
    this.blogsDocument = this.afs.doc('subscribers/' + id);
    return this.blogsDocument.update(data);
  }

  deleteSubscriber(id: string) {
    this.blogsDocument = this.afs.doc('subscribers/' + id);
    return this.blogsDocument.delete();
  }

  getPercentage(): Observable<number> {
    return this.percentage;
  }

  async googleSignin() {
    const provider = new auth.GoogleAuthProvider();
    const credential = await this.afAuth.auth.signInWithPopup(provider);
    return this.updateUserData(credential.user);
  }

  async signOut() {
    await this.afAuth.auth.signOut();
    this.router.navigate(['/']);
  }

  private updateUserData(user) {
    // Sets user data to firestore on login
    const userRef: AngularFirestoreDocument<User> = this.afs.doc(`users/${user.uid}`);

    const data = { 
      uid: user.uid,
      email: user.email, 
      displayName: user.displayName, 
      photoURL: user.photoURL
    }

    return userRef.set(data, { merge: true })
  }
}
