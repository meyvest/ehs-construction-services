import { Component, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MobileDesktopIdentifierServiceService } from '../services/mobile-desktop-identifier-service.service';
import { FirestoreService } from '../services/firestore.service';
import { Observable } from 'rxjs';
declare var $: any;

@Component({
  selector: 'app-single-blog',
  templateUrl: './single-blog.component.html',
  styleUrls: ['./single-blog.component.css']
})
export class SingleBlogComponent implements OnInit {
  isMobile: boolean;
  blog: any;
  id: any;

  constructor(private mobileDesktopService: MobileDesktopIdentifierServiceService, private firestoreService: FirestoreService, private route: ActivatedRoute) { 
    this.route.params.subscribe(res => { 
      this.id = res.id;
    });
  }

  ngOnInit() {
    window.scroll(0,0);
    this.firestoreService.getBlog(this.id).subscribe(data => {
      this.blog = data;
    });
    this.isMobile = this.mobileDesktopService.isMobile();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.isMobile = this.mobileDesktopService.isMobile();
  }

}
