import { Component, OnInit } from '@angular/core';
import { FirestoreService } from "../services/firestore.service";
import { Observable } from "rxjs";
declare var $: any;

@Component({
  selector: 'app-admin-subscribers',
  templateUrl: './admin-subscribers.component.html',
  styleUrls: ['./admin-subscribers.component.css']
})
export class AdminSubscribersComponent implements OnInit {
  subscribers: Observable<any[]>;
  subscribersList: any[];
  searchValue = "";
  emailStr: string;

  constructor(public firestoreService: FirestoreService) {}

  ngOnInit() {
    window.scroll(0, 0);
    this.subscribers = this.firestoreService.getSubscribersWithIDs();
    this.subscribers.subscribe(data => {
      this.subscribersList = data;
      this.emailStr = this.getEmailStr();
    })
  }

  getEmailStr() {
    var str = "mailto:";
    for(let i = 0; i < this.subscribersList.length; i++) {
      if (i == this.subscribersList.length - 1) {
        str += this.subscribersList[i].email;
      } else {
        str += this.subscribersList[i].email + ','
      }
    }
    return str;
  }

  deleteSubscriber(index) {
    this.firestoreService.deleteSubscriber(this.subscribersList[index].id)
  }

  onKey(event: any) {
    // without type info
    this.searchValue = event.target.value;
  }

}
