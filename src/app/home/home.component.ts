import { Component, OnInit, HostListener } from '@angular/core';
import { MobileDesktopIdentifierServiceService } from '../services/mobile-desktop-identifier-service.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirestoreService } from '../services/firestore.service';
declare var $: any;

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    isMobile: boolean;
    hasModalAppeared: boolean = false;
    form: FormGroup;

    constructor(private mobileDesktopService: MobileDesktopIdentifierServiceService, private formBuilder: FormBuilder, private firestoreService: FirestoreService) { }

    ngOnInit() {
        window.scroll(0,0);
        $('#navbar-item-mobile-home').addClass('active');
        this.isMobile = this.mobileDesktopService.isMobile();
        $('#navbar-desktop').attr("style", "display: none !important");
        this.form = this.formBuilder.group({
            email: [null, Validators.required],
            time_created: [new Date()]
        })
    }

    ngAfterViewInit(): void {
        $("#img-logo").attr("style","visibility: hidden !important");
        $("#img-logo").attr("src","assets/images/B-logo.png");
        setTimeout(function(){
            $('#email-modal').modal('show');
        }, 8888);
    }

    ngOnDestroy(): void {
        $('#navbar-item-mobile-home').removeClass('active');
        $('#navbar-desktop').attr("style", "display: flex !important");
        $("#img-logo").attr("style","visibility: show !important");
        $("#img-logo").attr("src","assets/images/W-logo.png");
        $('#text-home-details-phone').removeClass('text-dark').addClass('text-white');
    }

    @HostListener('window:resize', ['$event'])
        onResize(event) {
        this.isMobile = this.mobileDesktopService.isMobile();
        $('#navbar-desktop').attr("style", "display: none !important");
    }

    @HostListener("window:scroll", [])
    onScroll(): void {
        if (!this.isInViewPort('#carouselHome')) {
            $("#img-logo").attr("style","visibility: show !important");
            $('#text-home-details-phone').removeClass('text-white').addClass('text-dark');
        } else {
            $("#img-logo").attr("style","visibility: hidden !important");
            $('#text-home-details-phone').removeClass('text-dark').addClass('text-white');
        }
    }

    toggleCard(show: string, hide: string) {
        $('#' + show).attr("style", "display: flex !important;");
        $('#' + hide).attr("style", "display: none !important;");
    }

    validateEmail(email) {
        var regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        return regexp.test(String(email).toLowerCase());
    }

    showSubmitButton() {
        return this.validateEmail(this.form.controls['email'].value);
    }

    onSubmit() {
        var submitData = this.form.value;
        this.firestoreService.addSubscriber(submitData).then(() => {
            $('#email-modal').on('hidden.bs.modal', function (e) {
                $('#success-modal').modal('show');
            })
            $('#email-modal').modal('hide');
        })
    }

    isInViewPort(id: string) {
        var elementTop = $(id).offset().top;
        var elementBottom = elementTop + $(id).outerHeight();
        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height();
        return elementBottom > viewportTop && elementTop < viewportBottom;
    }
}
