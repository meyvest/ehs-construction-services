import { Component, OnInit } from "@angular/core";
import { FirestoreService } from "../services/firestore.service";
import { Observable } from "rxjs";
declare var $: any;

@Component({
  selector: "app-admin-contact",
  templateUrl: "./admin-contact.component.html",
  styleUrls: ["./admin-contact.component.css"]
})
export class AdminContactComponent implements OnInit {
  contacts: Observable<any[]>;
  contactsList: any[];
  contact: any;
  searchValue = "";
  sortKey = "";
  prevSortKeySelected = "";

  constructor(public firestoreService: FirestoreService) {}

  ngOnInit() {
    window.scroll(0, 0);
    this.contacts = this.firestoreService.getContactsWithIDs();
    this.contacts.subscribe(data => {
      this.contactsList = data;
    });
    $("#showContactModal").on("hidden.bs.modal", function(e) {
      $("#btn-delete-confirm").attr("style", "display: none !important");
      $("#btn-delete").attr("style", "display: block !important");
    });
  }

  getEmailStr() {
    return "mailto:" + this.contact.email;
  }

  updateContact(i: any) {
    this.contact = this.contactsList[i];
    const updateData = { read: true };
    this.firestoreService.updateContact(updateData, this.contact.id);
  }

  setUnreadFlag() {
    const updateData = { read: false };
    this.firestoreService.updateContact(updateData, this.contact.id);
  }

  deleteContact() {
    this.firestoreService.deleteContact(this.contact.id);
  }

  setDeleteFlag() {
    $("#btn-delete").attr("style", "display: none !important");
    $("#btn-delete-confirm").attr("style", "display: block !important");
  }

  switchFilterKey(id: string) {
    this.sortKey = id;
    this.triggerActiveDropdown(id);
  }

  triggerActiveDropdown(id: string) {
    // console.log(id);
    if (!this.prevSortKeySelected) this.prevSortKeySelected = id;

    $("#" + this.prevSortKeySelected)
      .removeClass("bg-primary")
      .removeClass("text-white");
    this.prevSortKeySelected = id;
    $("#" + id)
      .addClass("bg-primary")
      .addClass("text-white");
  }

  onKey(event: any) {
    // without type info
    this.searchValue = event.target.value;
  }
}
