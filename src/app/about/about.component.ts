import { Component, OnInit, HostListener } from '@angular/core';
import { MobileDesktopIdentifierServiceService } from '../services/mobile-desktop-identifier-service.service'
declare var $: any;

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  isMobile: boolean;

  constructor(private mobileDesktopService: MobileDesktopIdentifierServiceService) { }

  ngOnInit() {
    window.scroll(0,0);
    this.isMobile = this.mobileDesktopService.isMobile();
    $('#navbar-item-mobile-about').addClass('active');
  }

  ngOnDestroy(): void {
    $('#navbar-item-mobile-about').removeClass('active');
  }
  
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.isMobile = this.mobileDesktopService.isMobile();
  } 

}
