import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'subscribers'
})
export class SubscribersPipe implements PipeTransform {

  transform(items: any[], search: string): any {
    if (!items) {
      return null;
    } 

    // Check for sorting key, and search values
    if(search) items = this.searchValue(items, search);

    return items;
  }

  searchValue(items: any[], search: string) {
    var searchFunction = function(element, search) {
      var flag: boolean = false;
      if (element.email.toLowerCase().includes(search.toLowerCase())) {
        flag = true;
      } 
      return flag;
    }

    items = items.filter(singleItem => searchFunction(singleItem, search))
    return items;
  }

}
